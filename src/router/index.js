import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/login'
import Layout from '@/views/layout'
import Home from '@/views/home'
import PicList from '@/components/picList'
import PicCategory from '@/components/picCategory'
import PicManage from '@/views/picManage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'layout',
      component: Layout,
      children: [
        {
          path: '/home',
          name: 'home',
          component: Home,
          meta: { title: '首页' }
        },
        {
          path: '/picManage',
          redirect: '/picList',
          component: PicManage,
          meta: { title: '图库管理' },
          children: [
            {
              path: '/picManage/picList',
              name: 'picList',
              component: PicList,
              meta: { title: '图库列表' }
            },
            {
              path: '/picManage/picCategory',
              name: 'picCategory',
              component: PicCategory,
              meta: { title: '图库分类' }
            },
          ]
        }
      ]
    },
    
  ]
})
