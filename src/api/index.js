import request from '@/utils/request';

export function login (account) {
  return request({
    url: '/api/login' + account,
    methods: 'get',
  })
}

export function register (params) {
  return request({
    url: '/api/register',
    method: 'post',
    data: params
  })
}