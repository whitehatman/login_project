// 数据库连接配置
const mysql = require('mysql');
const connection = mysql.createConnection({
  host     : 'localhost',	//连接的数据库地址。（默认:localhost）
  user     : 'root',		//mysql的连接用户名
  password : 'admin',		// 对应用户的密码
  database : 'login_project'  		//所需要连接的数据库的名称（可选）
});

module.exports = connection
