//express_demo.js 文件
const express = require('express')
const routerApi = require('./router')

const app = express()
// 全局解析post请求的数据
app.use(express.json())

// 设置跨域
// app.all("*", function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*"); //设置允许跨域的域名，*代表允许任意域名跨域
//   res.header("Access-Control-Allow-Headers", "content-type"); //允许的header类型
//   res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS"); //跨域允许的请求方式
//   if (req.method.toLowerCase() == 'options')
//       res.send(200); //让options尝试请求快速结束
//   else
//       next();
// });

app.use('/api',routerApi)

app.listen(8081, function () {
  const host = 'localhost'
  const port = '8081'
  console.log("应用实例，访问地址为 http://%s:%s", host, port)

})