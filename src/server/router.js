const express = require('express');
const router = express.Router();
const api = require('./api');
 
router.get('/login', (req, res, next) => {
  api.login(req, res, next);
});

router.post('/register', (req, res, next) => {
  api.register(req, res, next);
});
module.exports = router;